console.log("Hello Mike")

let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This Pokemon tackled targetPokemon');
		console.log("targetPokemon's health is now reduce to _targetPokemonhealth_")
	},
	faint: function() {
		console.log("Pokemon fainted")
	}
}

function Pokemon(name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = 2 * level;

	//methods
	this.tackle = function(target) {
		console.log(`${this.name} tackled ${target.name}`);
		console.log("Target Pokemon's health is now reduced.")
	};
	this.faint = function() {
		console.log(`${this.name} fainted.`)
	}
}


let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

pikachu.tackle(rattata);
rattata.tackle(pikachu);


rattata.faint();


let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon = ["Snorlax","Pikachu","Squirtle","Bulbasaur"]
	friends: ["misty","brook","tracey"]
}




	